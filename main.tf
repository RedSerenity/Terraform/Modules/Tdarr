data "docker_registry_image" "Tdarr" {
	name = "haveagitgat/tdarr:latest"
}

resource "docker_image" "Tdarr" {
	name = data.docker_registry_image.Tdarr.name
	pull_triggers = [data.docker_registry_image.Tdarr.sha256_digest]
}

module "Tdarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Tdarr.latest

	networks = [{ name: var.docker_network, aliases: ["tdarr.${var.internal_domain_base}"] }]
  ports = [
		{ internal: 8265, external: 9611, protocol: "tcp" },
		{ internal: 8266, external: 9612, protocol: "tcp" }
	]
	volumes = [
		{
			host_path = "${var.docker_data}/Tdarr/Server"
			container_path = "/app/server"
			read_only = false
		},
		{
			host_path = "${var.docker_data}/Tdarr/Configs"
			container_path = "/app/configs"
			read_only = false
		},
		{
			host_path = "${var.docker_data}/Tdarr/Logs"
			container_path = "/app/logs"
			read_only = false
		},
		{
			host_path = "${var.media}"
			container_path = "/media"
			read_only = false
		},
		{
			host_path = "${var.transcode_cache}"
			container_path = "/temp"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}",
		"serverIP": "0.0.0.0",
		"serverPort": "8266",
		"webUIPort": "8265"
	}

	stack = var.stack
}